import React from 'react';

export const Footer = () => {

    return (
        <footer>
            <div className="container">
                <div className="row footer">
                    <span className="col-s5"> {new Date().getFullYear()} &#169; copyright Kristina Butkovic </span>
                </div>
            </div>
        </footer>
    );
};

